#!/bin/bash

set -exu

DEBUG=true
. /srv/jenkins/bin/common-functions.sh
common_init "$@"

REPO_URL=https://salsa.debian.org/josch/mmdebstrap-jenkins

TARGET=/srv/mmdebstrap-jenkins

sudo mkdir -p $TARGET/
sudo chown -R jenkins:jenkins $TARGET/

if [ ! -d $TARGET/mmdebstrap-jenkins ]; then
	git clone --recurse $REPO_URL $TARGET/mmdebstrap-jenkins
else
	git -C $TARGET/mmdebstrap-jenkins status || /bin/true
	git -C $TARGET/mmdebstrap-jenkins fetch
	git -C $TARGET/mmdebstrap-jenkins reset --hard origin/master
	git -C $TARGET/mmdebstrap-jenkins submodule update --init
fi

sh $TARGET/mmdebstrap-jenkins/run.sh

# vim: set sw=0 noet :

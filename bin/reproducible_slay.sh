#!/bin/bash

# Copyright 2015-2016 Holger Levsen <holger@layer-acht.org>
# released under the GPLv2

set +e

# usually called by /srv/jenkins/bin/reproducible_cleanup_nodes.sh
# this script just kills everyone…
sudo killall -9 timeout	# all builds are done using timeout
sudo slay 1111
sudo slay 2222
pgrep -u 1111,2222
# only slay jenkins on the build nodes…
if [ "$HOSTNAME" != "jenkins" ] ; then
	# …but cleanup build dirs first, as the last slay will also kill this running script...
	REP_RESULTS=/srv/reproducible-results
	for PATTERN in 'tmp.*' 'r-b-build.*' ; do
		find $REP_RESULTS/rbuild-debian -maxdepth 1 -type d -name "$PATTERN" -exec rm -rv --one-file-system {} \; || true
	done
	# finally, slay jenkins
	sudo slay jenkins
fi

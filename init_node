#!/bin/bash
#
# Copyright 2014-2023 Holger Levsen <holger@layer-acht.org>
#
# released under the GPLv2
#
# initial setup for new nodes
# - this script must be run as root
# - it's also assumed that this script is once manually scp'ed to the new node
#   and run there manually once :)

# let's check our assumptions first…
if [ "$(id -u)" -ne 0 ] ; then
	echo This needs to be run as root… exiting.
	exit 1
fi

START=$(date +'%s')
GIT_REPO="https://salsa.debian.org/qa/jenkins.debian.net.git"

echo
date -u
set -e
export LANG=C
set -x
addgroup --system jenkins
adduser --system --shell /bin/bash --home /var/lib/jenkins --ingroup jenkins --disabled-login jenkins
addgroup --system jenkins-adm
adduser --system --shell /bin/bash --home /home/jenkins-adm --ingroup jenkins-adm --disabled-login jenkins-adm
usermod -G jenkins jenkins-adm
which sudo || apt-get install -y sudo
echo '%jenkins-adm ALL= NOPASSWD: ALL' > /etc/sudoers.d/jenkins-adm # will be overwritten later
chown jenkins-adm:jenkins-adm /home/jenkins-adm
cd ~jenkins-adm
[ -x /usr/bin/git ] || apt-get install -y git
su jenkins-adm -l -c "git clone $GIT_REPO"
cd jenkins.debian.net
su jenkins-adm -l -c "git -C ~jenkins-adm/jenkins.debian.net config pull.ff only"
mkdir -p /var/log/jenkins/
chown jenkins:jenkins /var/log/jenkins/
set +x
set +e

END=$(date +'%s')
DURATION=$(( END - START ))
HOUR=$(( DURATION/3600 ))
MIN=$(( (DURATION-HOUR*3600)/60 ))
SEC=$(( DURATION-HOUR*3600-MIN*60 ))
echo "$(date -u) - total duration: ${HOUR}h ${MIN}m ${SEC}s."
echo
echo "Now finish the deployment by running:"
echo -n "        cd ~jenkins-adm/jenkins.debian.net/ ;"
# this assumes that the user you are running this script can sudo to root.
# "usually" sudo-to-root is passwordless but sudo-to-others is not, so workaround that
echo "        sudo sudo -u jenkins-adm ./update_jdn.sh 2>&1 | sudo tee -a /var/log/jenkins/update_jdn.log"

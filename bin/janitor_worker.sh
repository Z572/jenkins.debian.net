#!/bin/bash

set -e

docker run --rm --privileged \
	--memory=20g \
	-v /srv/janitor:/srv/janitor \
	-e BUILD_URL="${BUILD_URL}" \
	-e NODE_NAME="${NODE_NAME}" \
	--cpu-shares 86 \
	--tmpfs /tmp:rw,exec,dev,suid \
	--tmpfs /var/lib/schroot/session:rw,exec,dev,suid \
	--tmpfs /var/lib/schroot/union/overlay:rw,exec,dev,suid \
	--tmpfs /var/lib/sbuild/build:rw,exec,dev,suid \
	eu.gcr.io/debian-janitor/worker:latest \
	--listen-address=0.0.0.0 \
	--port=8080 \
	--base-url=https://janitor.debian.net/runner/ \
	--credentials=/srv/janitor/credentials.json \
	--tee
